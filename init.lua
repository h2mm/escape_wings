-- Wings of Escape Mod

-- Wing effect

local effect = {
	initial_properties = {
		physical = true,
		collisionbox = {-0.6, 0, -0.6, 0.6, 2.2, 0.6},
		visual = "mesh",
		mesh = "escape_wings_effect.obj",
		textures = {"escape_wings_effect.png"},
		use_texture_alpha = true,
		glow = LIGHT_MAX,
	},
	user = nil,
	last_pos = {x=0, y=0, z=0},
	time = 0,
	last_update = 0,
	elapsed = 0,
	sound = nil,
	removed = false,
	auto = false
}

function take_wings()
	local player_name = placer and placer:get_player_name() or ""
	if (creative and creative.is_enabled_for and
		creative.is_enabled_for(player_name)) then
			return false
	end
	if minetest.settings:get_bool("escape_wings_reusable") == true then
		return false
	end
	return true
end


function effect.on_rightclick(self, clicker)
	if not clicker or not clicker:is_player() then
		return
	end
	local name = clicker:get_player_name()
	if not self.user then
		local attach = clicker:get_attach()
		if attach and attach:get_luaentity() then
			local luaentity = attach:get_luaentity()
			if luaentity.user then
				luaentity.user = nil
			end
			clicker:set_detach()
		end
		self.user = name
		clicker:set_attach(self.object, "",
			{x = 0, y = 1, z = 0}, {x = 0, y = 0, z = 0})
		player_api.player_attached[name] = true
		clicker:set_look_horizontal(self.object:get_yaw())
	end
	self.object:set_acceleration({x = 0, y = -9.81, z = 0})

	self.last_pos = self.object:get_pos()
	self.last_update = minetest.get_us_time()
	self.time = self.last_update + 20000000

	self.sound = minetest.sound_play("escape_wings_rotating", {
		pos = clicker:get_pos(),
		max_hear_distance = 100,
		gain = 10.0,
	})
end

function effect.on_detach_child(self, child)
	self.user = nil
	self.auto = false

	self.removed = true
	if child and self.time > minetest.get_us_time() then
		if take_wings() then
			local inv = child:get_inventory()
			local leftover = inv:add_item("main", "escape_wings:item")
			if not leftover:is_empty() then
				minetest.add_item(self.object:get_pos(), leftover)
			end
		end
		
		minetest.sound_play("escape_wings_unteleport", {
			pos = child:get_pos(),
			max_hear_distance = 100,
			gain = 10.0,
		})
	end

	if self.sound then
		minetest.sound_stop(self.sound)
	end

	minetest.after(0.1, function()
		self.object:remove()
	end)
end

function effect.on_activate(self, staticdata, dtime_s)
	if staticdata then
		self.v = tonumber(staticdata)
	end
	self.last_v = self.v
end

function effect.get_staticdata(self)
	return tostring(self.v)
end

function effect.on_punch(self, puncher)
	if not puncher or not puncher:is_player() or self.removed then
		return
	end

	if self.user then
		self.user = nil
		puncher:set_detach()
		if name then
			player_api.player_attached[name] = false
		end
	end
end


function effect.on_step(self, dtime)
	local pos = self.object:get_pos()
	local nouw = minetest.get_us_time()
	local elapsed = nouw - self.last_update
	self.elapsed = self.elapsed + elapsed
	if not self.removed then
		if (pos.x ~= self.last_pos.x) or (pos.y ~= self.last_pos.y) or (pos.z ~= self.last_pos.z) then
			self.removed = true
			if take_wings() then
				minetest.add_item(self.object:get_pos(), "escape_wings:item")
			end

			minetest.after(0.1, function()
				self.object:remove()
			end)
		end
		if (self.time > 0) and (nouw > self.time) then
			self.removed = true

			if self.user then
				player_api.player_attached[self.user] = false

				local player = minetest.get_player_by_name(self.user)
				local pos_ = beds.spawn[self.user]
				if pos_ then
					self.object:set_pos(pos_)
					player:set_pos(pos_)
					minetest.sound_play("escape_wings_teleported", {
						pos = pos_,
						max_hear_distance = 100,
						gain = 10.0,
					})
				end
			end

			minetest.after(0.1, function()
				self.object:remove()
			end)
		else
			self.object:set_yaw(self.object:get_yaw() + elapsed*20/(20500000-self.elapsed))
		end
	-- elseif minetest.settings:get_bool("escape_wings_reusable") then
		-- minetest.add_item(self.object:get_pos(), leftover)
	end
	self.last_update = nouw
end

minetest.register_on_joinplayer(function(player)
	if not beds.spawn[player:get_player_name()] then
		beds.spawn[player:get_player_name()] = player:get_pos()
		beds.set_spawns()
	end
end)

minetest.register_entity("escape_wings:effect", effect)

-- Wing item

minetest.register_craftitem("escape_wings:item", {
	description = "Wings of Escape",
	inventory_image = "escape_wings_item.png",
	on_use = function(itemstack, placer, pointed_thing)
		minetest.after(0.5, function()
			effect = minetest.add_entity(placer:get_pos(), "escape_wings:effect")
			if effect then
				if placer then
					effect:set_yaw(placer:get_look_horizontal())
				end
				effect:right_click(placer)
			end
		end)
		if take_wings() then
			itemstack:take_item()
		end
		return itemstack
	end,
})

minetest.register_craft({
	output = 'escape_wings:item',
	recipe = {
		{"default:steel_ingot", "", "default:steel_ingot"},
		{"default:steel_ingot", "default:gold_ingot", "default:steel_ingot"},
		{"default:steel_ingot", "", "default:steel_ingot"},
	}
})
